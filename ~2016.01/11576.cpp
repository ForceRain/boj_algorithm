#include <iostream>
#include <cstdio>
#include <cmath>

using namespace std;

int pos[31];

int main(void)
{
	int A,B,M;
	scanf("%d%d",&A,&B);
	scanf("%d",&M);
	for (int i=M-1;i>=0;i--)
		scanf("%d",&pos[i]);
	long long tot_sum=0;
	for (int i=M-1;i>=0;i--)
		tot_sum+=(pow(A,i))*pos[i];
	long long ruler = 1;
	while ( ruler <= tot_sum )	ruler*=B;
	ruler/=B;
	while ( ruler !=0 )
	{
		printf("%d ",tot_sum/ruler);
		tot_sum%=ruler;
		ruler/=B;
	}
	printf("\n");

	return 0;
}