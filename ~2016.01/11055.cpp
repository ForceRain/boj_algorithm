#include <iostream>
#include <cstdio>

using namespace std;

int space[1001];
int dp[1001];

int main(void)
{
	int N;
//	freopen("test.txt","r",stdin);
	scanf("%d",&N);

	for (int i=0;i<N;i++)	
	{
		scanf("%d",&space[i]);
		dp[i]=space[i];
	}

	int max_sum = space[0];
	for ( int i=1;i<N;i++ )
	{
		int max = 0x80000000;
		for ( int j = i-1 ; j >= 0 ;j-- )
		{
			if ( space[i] > space[j] )
			{
				int val = dp[j] + space[i];
				if ( max < val )
					max = val;
			}
		}
		if ( dp[i] < max )
			dp[i] = max;
		
		if ( dp[i] > max_sum )
			max_sum = dp[i];
	}
	printf("%d\n",max_sum);

	return 0;
}