#include <iostream>
#include <cstdio>

using namespace std;

int space[1000001];
int values[20001];

int main(void)
{
	int N,S;
	scanf("%d%d",&N,&S);
	for (int i=0;i<N;i++){
		scanf("%d",&values[i]);
		space[values[i]]=1;
	}
	long long cnt=0;
	for (int j=0;j<N;j++){
		for (int k=S;k>=0;k--){
			if (((k-values[j])>=0) && (space[k-values[j]]==1) && ((k-values[j])!=values[j])){
		//		cout<<values[j]<<endl;
				cnt++;
			}
		}
	}
	printf("%lld\n",cnt/2);

	return 0;
}