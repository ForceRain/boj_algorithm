#include <iostream>
#include <cstdio>
#include <vector>

using namespace std;

unsigned long long dp[1001][1001];
vector<pair<int,int> > container;

int main(void)
{
	freopen("test.txt","r",stdin);
	for (int i=0;i<1001;i++)
	{
		for (int j=0;j<=i;j++)
		{
			if (i==j || j==0)
				dp[i][j]=1;
			else
				dp[i][j]=dp[i-1][j]+dp[i-1][j-1];
		}
	}

	unsigned long long N;
	scanf("%llu",&N);
	int cnt=0;
	for (int i=0;i<1001;i++)
	{
		for (int j=0;j<1001;j++)
		{
			if (N==dp[i][j])
			{
				cnt++;
				container.push_back(pair<int,int>(i,j));
			}
		}
	}
	int sz = container.size();
	printf("%d\n",sz);
	for (int i=0;i<sz;i++)
		printf("%d %d\n",container[i].first,container[i].second);

	return 0;
}