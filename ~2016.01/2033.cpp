#include <iostream>
#include <cstdio>

using namespace std;

int res[100];

int main(void)
{
	int test;
	freopen("test.txt","r",stdin);
	scanf("%d",&test);
	for (int k=0;k<test;k++)
	{
		unsigned long long N;
		scanf("%llu",&N);
		int resPtr = 0;

		while ( N > 10 )
		{
			int upper = N%10;
			int going = 0;
			if ( upper >= 5 )
				going = 1;

			res[resPtr++] = 0;
			N/=10;
			N +=going;
		}
		res[resPtr]=N;
		for ( int j=resPtr;j>=0;j-- )
			printf("%d",res[j]);
		printf("\n");
	}
	
	return 0;
}