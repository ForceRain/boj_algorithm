#include <iostream>
#include <cstdio>
#include <cstring>
#include <stack>
#include <cstdlib>
using namespace std;

char in1[1001];
char in2[1001];
char res1[1001];
char res2[1001];

int main(void)
{
	freopen("test.txt","r",stdin);
	scanf("%s",in1);
	scanf("%s",in2);
	int len1 = strlen(in1);
	int len2 = strlen(in2);
	stack<char> st1,st2;
	int p1 = len1 - 1 , p2 = len2 - 1;
	bool same_check = false;

	while ( ( p1 >= 0 ) && ( p2 >= 0 ) )
	{
		if ( in1[p1] > in2[p2] )
			st1.push( in1[p1] );
		else if ( in1[p1] < in2[p2] )
			st2.push( in2[p2] );
		else
		{
			st1.push( in1[p1] );
			st2.push( in2[p2] );
			same_check = true;
		}
		p1--;
		p2--;
	}
	while ( p1 >= 0 )
		st1.push( in1[p1--] );
	while ( p2 >= 0 )
		st2.push( in2[p2--] );

	while ( !st1.empty() )
	{
		res1[++p1] = st1.top();
		st1.pop();
	}
	while ( !st2.empty() )
	{
		res2[++p2] = st2.top();
		st2.pop();
	}
	int r1 = atoi(res1);
	int r2 = atoi(res2);

	if ( same_check )
	{
		printf("%d\n",r1);
		printf("%d\n",r2);
	}
	else
	{
		if ( r1 == 0 )
			printf("YODA\n");
		else
			printf("%d\n",r1);

		if ( r2 == 0 )
			printf("YODA\n");
		else
			printf("%d\n",r2);
	}

	return 0;
}