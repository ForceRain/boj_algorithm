#include <iostream>
#include <cstdio>

using namespace std;

int space[251];

int main(void)
{
	int N,out=0;
	scanf("%d",&N);
	for (int i=0;i<=N;i++)	space[i]=i;

	int moving_ptr=1;
	int moving_val=1;
	space[moving_ptr]=-1;
	while (true)
	{
		bool check=true;
		int dest_ptr=((moving_ptr+moving_val)%N == 0)? N :(moving_ptr+moving_val)%N;
		int tmp = moving_val;

		moving_val = space[dest_ptr];
		space[dest_ptr] = tmp;
		moving_ptr = dest_ptr;

		for (int j=1;j<=N;j++)
		{
			if (space[j]==-1)
			{
				check=false;
				break;
			}
		}
		if (check)
		{
			out = space[dest_ptr];
			break;
		}
	}
	printf("%d\n",out);

	return 0;
}