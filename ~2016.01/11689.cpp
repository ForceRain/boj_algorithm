#include <iostream>
#include <cstdio>

using namespace std;

int ea(int a,int b)
{
	if (b==0)
		return a;
	else
		return ea(b,a%b);
}

int main(void)
{
	int N;
	scanf("%d",&N);
	int cnt=0;
	for (int i=1;i<=N;i++)
	{
		int val = ea(i,N);
		if (val == 1)
			cnt++;
	}
	printf("%d\n",cnt);

	return 0;
}