#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

char input_buffer[1010];

int get_val(char c)
{
	if ( '0'<=c && c<='9' )
		return c-'0';
	else if ( 'A'<=c && c<='F' )
		return (c-'A')+10;
}

int main(void)
{
	freopen("test.txt","r",stdin);
	while (scanf("%s",input_buffer)!=EOF)
	{
		int len = strlen(input_buffer);
		for (int i=0;i<len;i+=2)
		{
			int val = get_val(input_buffer[i])*16 + get_val(input_buffer[i+1]);
			printf("%c",val);
		}
		printf("\n");
	}

	return 0;
}