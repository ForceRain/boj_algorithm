#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

int dp[5001];
char test_buffer[100];

int main(void)
{
	int N,M;
	freopen("test.txt","r",stdin);
	for ( int i=1;i<=5000;i++ )
	{
		int arr[10]={0};
		int check_val = i;
		sprintf(test_buffer,"%d",check_val);
		int len = strlen(test_buffer);
		for (int j=0;j<len;j++)
			arr[test_buffer[j]-'0']++;
		int set_val = 0;
		for (int j=0;j<10;j++)
			if ( arr[j] > 1 )	set_val = 1;
		dp[i]=dp[i-1]+set_val;
	}

	while (scanf("%d%d",&N,&M)!=EOF)
	{
		int dp_val = dp[M]-dp[N-1];
		int sum_val = M-N+1;
		printf("%d\n",sum_val-dp_val);
	}
	return 0;
}