#include <iostream>
#include <cstdio>
#include <algorithm>

using namespace std;

int space[100001];
int vals[100001];

int b_search(int s,int e,int val)
{
	int mid = (s+e)/2;
	if (s<=e){
		if ( space[mid] > val )
			return b_search(s,mid-1,val);
		else if ( space[mid] < val )
			return b_search(mid+1,e,val);
		else
			return mid;
	}
	return mid;
}

int main(void)
{
	int N,M;
	scanf("%d%d",&N,&M);
	int cnt=1;
	for (int i=0;i<N;i++){
		int pos;
		scanf("%d",&pos);
		space[i]=pos;
		vals[i]=cnt++;
	}
	space[N]=-1;
	vals[N]=cnt;
	sort(space,space+N);
	for (int i=0;i<M;i++)
	{
		int p,q;
		scanf("%d%d",&p,&q);
		if (p>q)
		{
			int tmp = p;
			p = q;
			q = tmp;
		}
		int p1 = b_search(0,N,p);
		int p2 = b_search(0,N,q);
		cout<<p1<<","<<p2<<endl;
		
		if ( space[p1]==p || space[p2]==q )
			printf("%d\n",vals[p2]-vals[p1]+1);
		else
			printf("%d\n",vals[p2]-vals[p1]);
	}

	return 0;
}