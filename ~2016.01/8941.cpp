#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>

using namespace std;

char input_buffer[100];
double origin[256];

int main(void)
{
	int N;
	origin['C']=12.01;
	origin['H']=1.008;
	origin['O']=16.00;
	origin['N']=14.01;
	scanf("%d",&N);

	for (int i=0;i<N;i++)
	{
		scanf("%s",input_buffer);
		double total_sum=0.0;
		
		int len = strlen(input_buffer);
		for (int j=0;j<len;j++)
		{
			char buf[100]={'1'};
			char pos;
			int buf_ptr=0;

			if (('A'<=input_buffer[j]) && (input_buffer[j]<='Z'))
			{
				pos = input_buffer[j];
				j++;
				while ( ('0'<=input_buffer[j]) && (input_buffer[j]<='9') )
					buf[buf_ptr++]=input_buffer[j++];
				int numo = atoi(buf);
				total_sum+=numo*origin[pos];
				j--;
			}
		}
		printf("%.3lf\n",total_sum);
	}

	return 0;
}