#include <iostream>
#include <cstdio>

using namespace std;

int dp[1001];
int values[1001];

int main(void)
{
	int N;
	freopen("test.txt","r",stdin);

	scanf("%d",&N);
	for (int i=0;i<N;i++)	scanf("%d",&values[i]);
	for ( int i=0;i<N;i++ )	dp[i] = 1;
	int max = -1;

	for ( int i=0;i<N;i++ )
	{
		for ( int j=i;j>=0;j-- )
		{
			if ( values[j] > values[i] )
			{
				if ( dp[i] < (dp[j] +1) )
					dp[i] = (dp[j] +1);
			}
		}
		if (dp[i] > max)
			max = dp[i];
	}
	
	printf("%d\n",max);
	
	return 0;
}