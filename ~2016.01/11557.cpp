#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

bool my_compare(pair<string,int> a, pair<string,int> b)
{
	if (a.second>b.second)
		return true;
	else return false;
}

char n_buf[200];

int main(void)
{
	int test,val,loop;
	scanf("%d",&test);

	for (int i=0;i<test;i++)
	{
		vector< pair<string,int> > container;
		scanf("%d",&loop);
		for (int j=0;j<loop;j++)
		{
			cin>>n_buf>>val;
			container.push_back(pair<string,int>(n_buf,val));
		}
		sort(container.begin(),container.end(),my_compare);
		printf("%s\n",container[0].first.c_str());
	}
	
	return 0;
}