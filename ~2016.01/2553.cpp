#include <iostream>
#include <cstdio>

using namespace std;

int main(void)
{
	int N;
	scanf("%d",&N);
	int res = 1;
	for (int i=1;i<=N;i++)
	{
		res = res*i;
		if ( res>=10 )
			res = (res%10==0)?1:res%10;
	}
	printf("%d\n",res);

	return 0;
}