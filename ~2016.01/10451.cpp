#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

int trans[1001][1001];

int main(void)
{
	int test;
	freopen("test.txt","r",stdin);
	scanf("%d",&test);

	for (int i=0;i<test;i++)
	{
		memset(trans,0,sizeof(trans));
		int N;
		scanf("%d",&N);

		for (int j=1;j<=N;j++)
		{
			int val;
			scanf("%d",&val);
			trans[j][val]=1;
		}

		for ( int l=1;l<=N;l++ )
		{
			for ( int k=1;k<=N;k++ )
			{
				for ( int j=1;j<=N;j++ )
				{
					if ( trans[k][l]==1 && trans[l][j]==1 )
					{
						cout<<k<<","<<l<<" + "<<l<<","<<j<<" = "<<k<<","<<j<<endl;
						trans[k][j]=1;
					}
				}
			}
		}
		int cnt=0;
		for ( int j=1;j<=N;j++ )
			if (trans[j][j]==1)
				cnt++;
		printf("%d\n",cnt);
	}

	return 0;
}