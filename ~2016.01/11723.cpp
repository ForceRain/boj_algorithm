#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

int SET[21];
char input_buf[100];

int main(void)
{
	int M;
	freopen("test.txt","r",stdin);
	scanf("%d",&M);

	for (int i=0;i<M;i++)
	{
		int num;
		scanf("%s",input_buf);

		if (strcmp(input_buf,"add")==0)
		{
			scanf("%d",&num);
			SET[num]=1;
		}
		else if (strcmp(input_buf,"remove")==0)
		{
			scanf("%d",&num);
			SET[num]=0;
		}
		else if (strcmp(input_buf,"check")==0)
		{
			scanf("%d",&num);
			if (SET[num]==1)
				printf("1\n");
			else
				printf("0\n");
		}
		else if (strcmp(input_buf,"toggle")==0)
		{
			scanf("%d",&num);
			if (SET[num]==1)
				SET[num]=0;
			else
				SET[num]=1;
		}
		else if (strcmp(input_buf,"all")==0)
		{
			for (int j=0;j<21;j++)
				SET[j]=1;
		}
		else if (strcmp(input_buf,"empty")==0)
		{
			for (int j=0;j<21;j++)
				SET[j]=0;
		}
	}

	return 0;
}