#include <iostream>
#include <cstdio>

using namespace std;

int main(void)
{
	freopen("test.txt","r",stdin);
	int Umap[26],Lmap[26];
	for (int i=0;i<26;i++)
	{
		Umap[i]='A'+i;
		Lmap[i]='a'+i;
	}

	char c;
	while ( (c=getchar()) != EOF )
	{
		if ( ('a'<= c && c<='z') )
		{
			if ( (c-'a'+13)>25 )
				printf("%c",Lmap[(c-'a'+13)%26]);
			else
				printf("%c",Lmap[(c-'a'+13)]);
		}
		else if ( ( 'A'<= c && c<='Z' ) )
		{
			if ( (c-'A'+13)>25 )
				printf("%c",Umap[(c-'A'+13)%26]);
			else
				printf("%c",Umap[(c-'A'+13)]);
		}
		else
			printf("%c",c);
	}

	return 0;
}