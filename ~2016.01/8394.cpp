#include <iostream>
#include <cstdio>

using namespace std;

int main(void)
{
	int N;
	scanf("%d",&N);

	if ( N == 1 )
		printf("1\n");
	else if ( N == 2 )
		printf("2\n");
	else
	{
		int n1 = 1;
		int n2 = 2;
		int n3 = 0;
		for ( int i=3;i<=N;i++ )
		{
			n3 = n1+n2;
			n1 = n2;
			n2 = n3%10;
		}
		printf("%d\n",n3%10);
	}

	return 0;
}