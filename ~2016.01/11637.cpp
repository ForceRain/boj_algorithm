#include <iostream>
#include <cstdio>

using namespace std;

int values[100];

int main(void)
{
	int T;
	freopen("test.txt","r",stdin);
	scanf("%d",&T);

	for (int i=0;i<T;i++)
	{
		int n;
		scanf("%d",&n);
		int tot_sum=0;
		int max = -1;
		int max_p = -1;
		for (int k=0;k<n;k++){
			scanf("%d",&values[k]);
		//	values[k]++;
			tot_sum+=values[k];
			if (max<values[k]){
				max = values[k];
				max_p = k;
			}
		}
		int val = values[0];
		bool nope = true;
		for (int j=1;j<n;j++){
			if (val!=values[j]){
				nope = false;
				break;
			}
		}
		if (nope)
			printf("no winner\n");
		else
		{
			if (tot_sum<=2*values[max_p])
				printf("majority winner %d\n",max_p+1);
			else
				printf("minority winner %d\n",max_p+1);
		}
	}

	return 0;
}