#include <iostream>
#include <cstdio>

using namespace std;

int space[1000001];
int val[100001];
bool visited[1000001];

int main(void)
{
	int N;
	freopen("test.txt","r",stdin);
	scanf("%d",&N);

	for (int i=0;i<N;i++)
	{
		scanf("%d",&val[i]);
		if (visited[val[i]])
			space[val[i]]++;

		visited[val[i]] = true;
		for ( int j=2*val[i];j<1000001;j+=val[i] )
			space[j]++;
	}
	for (int i=0;i<N;i++)
		printf("%d\n",space[val[i]]);
	
	return 0; 
}