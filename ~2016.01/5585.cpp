#include <iostream>
#include <cstdio>

using namespace std;

int main(void)
{
	int val;
	int coin_val[6]={500,100,50,10,5,1};
	scanf("%d",&val);
	int cnt=0;
	val = 1000-val;
	for (int i=0;i<6;i++)
	{
		cnt+=(val/coin_val[i]);
		val=val%coin_val[i];
	}
	printf("%d\n",cnt);

	return 0;
}