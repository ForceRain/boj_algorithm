#include <iostream>
#include <cstdio>

using namespace std;

int main(void)
{
	int N;
	scanf("%d",&N);

	int a1=1,a2=2;
	if (N==1)
		printf("1\n");
	else if (N==2)
		printf("2\n");
	else
	{
		int a3 = 0;
		for (int i=2;i<N;i++)
		{
			a3 = (a1%10007)+(a2%10007);
			a1 = a2;
			a2 = a3;
		}
		printf("%d\n",a3%10007);
	}
	return 0;
}