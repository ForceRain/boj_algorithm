#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

char input_stream[100];

int main(void)
{
	scanf("%s",input_stream);

	if ( strncmp(input_stream,"0x",2)==0 )
	{
		int val;
		sscanf(input_stream+2,"%x",&val);
		printf("%d\n",val);
	}
	else if ( input_stream[0]=='0' )
	{
		int val;
		sscanf(input_stream+1,"%o",&val);
		printf("%d\n",val);
	}
	else
		printf("%s\n",input_stream);

	return 0;
}