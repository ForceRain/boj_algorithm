#include <iostream>
#include <cstdio>

using namespace std;

char out_buffer[100];

int main(void)
{
	int N,F;
	scanf("%d%d",&N,&F);

	int st_val = (N/100)*100;
	while (true)
	{
		if ((st_val%F)==0)
		{
			printf("%02d\n",st_val%100);
			break;
		}
		st_val++;
	}

	return 0;
}