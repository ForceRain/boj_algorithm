#include <iostream>
#include <cstdio>

using namespace std;

int row[1001];
int dp[1001][1001];

int main(void)
{
	int N;
	freopen("test.txt","r",stdin);
	scanf("%d",&N);
	int ptr = 0;
	
	for (int i=0;i<N;i++)
	{
		int a,b;
		scanf("%d%d",&a,&b);
		if ( i == 0 )
			row[ptr++]=a;
		row[ptr++]=b;
	}

	for ( int i=0;i<=N;i++ )	dp[i][i] = 0;

	for ( int di = 1; di <= N-1; di++ )
	{
		for ( int i=1; i<= N - di ;i++ )
		{
			int j = i+di;
			dp[i][j] = 0x20000000;

			for ( int k = i; k<=j-1;k++ )
			{
				int val = dp[i][k] + dp[k+1][j] + row[i-1]*row[k]*row[j];
				if ( val < dp[i][j] )
					dp[i][j] = val;
			}
		}
	}
	printf("%d\n",dp[1][N]);

	return 0;
}