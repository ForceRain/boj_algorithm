#include <iostream>
#include <list>
#include <cstdio>
#include <string>

using namespace std;

char input_stream[20];

int main(void)
{
	int N;
	freopen("test.txt","r",stdin);
	scanf("%d",&N);

	for (int i=0;i<N;i++)
	{
		list<string> listContainer;
		int num,cmd;
		scanf("%d%d",&num,&cmd);
		for ( int j=0;j<num;j++ )
		{
			scanf("%s",input_stream);
			listContainer.push_back(string(input_stream));
		}
		for ( int j=0;j<cmd;j++ )
		{
			int from,to;
			scanf("%d%d",&from,&to);
			list<string>::iterator it = listContainer.begin();
			for (int k=1;k<from;k++)	it++;
			string tmp = (*it);
			list<string>::iterator at = listContainer.begin();
			for (int k=1;k<to;k++)	at++;
			listContainer.insert(at,tmp);
			listContainer.erase(it);
		}
		list<string>::iterator rt = listContainer.begin();
		list<string>::iterator ed = listContainer.end();
		while ( rt != ed )	{
			printf("%s ",(*rt).c_str());
			rt++;
		}
		printf("\n");
	}

	return 0;
}