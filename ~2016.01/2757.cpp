#include <iostream>
#include <cstdio>
#include <stack>

using namespace std;

char mapping[100];

int main(void)
{
	for (int i=1;i<=26;i++)	mapping[i]='A'+(i-1);
	freopen("test.txt","r",stdin);

	while (true)
	{
		stack<int> container;
		int a,b;
		scanf("R%dC%d\n",&a,&b);

		if ( a==0 && b==0 )
			break;
		
		while ( b > 0 )
		{
			container.push( (b%26)+1 );
			b = b / 26;
		}
		while (!container.empty())
		{
			printf("%c",mapping[container.top()]);
			container.pop();
		}
		printf("%d\n",a);
	}
	return 0;
}
