#include <iostream>
#include <map>
#include <cstdio>
#include <string>

using namespace std;

char input_buffer[100];
map < string, int> container;

int main(void)
{
	int N,cookie_num=0,angry=0;
	scanf("%d",&N);

	for (int i=0;i<N;i++)
	{
		scanf("%s",input_buffer);
		map<string,int >::iterator it=container.find(string(input_buffer));
		if ( it != container.end() )
		{
			if ( it->second > (cookie_num - it->second) )
				angry++;
			
			it->second++;
			cookie_num++;
		
		}
		else
		{
			container.insert(pair<string,int>(string(input_buffer),1));
			cookie_num++;
		}
	//	cookie_num++;
	}
	printf("%d\n",angry);
	return 0;
}