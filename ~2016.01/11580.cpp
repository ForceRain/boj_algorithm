#include <iostream>
#include <cstdio>

using namespace std;

int space[2011][2011];
char input_buffer[1001];

int main(void)
{
	int px=1000,py=1000;
	int len;
	scanf("%d",&len);
	scanf("%s",input_buffer);
	int cnt=1;
	space[px][py]=1;

	for (int i=0;i<len;i++)
	{
		if (input_buffer[i]=='W')	px--;	
		if (input_buffer[i]=='E')	px++;
		if (input_buffer[i]=='S')	py--;
		if (input_buffer[i]=='N')	py++;

		if (space[px][py]==0){
			space[px][py]=1;
			cnt++;
		}
	}
	printf("%d\n",cnt);

	return 0;
}