#include <iostream>
#include <cstdio>

using namespace std;

int space[2000001];

int main(void)
{
	int N,endTime;
	scanf("%d%d",&N,&endTime);

	for (int i=0;i<N;i++)
	{
		int val;
		scanf("%d",&val);
		for (int j=val;j<=endTime;j+=val)	space[j]=1;
	}
	int cnt =0;
	for (int i=1;i<=endTime;i++)
		if (space[i]==1)	cnt++;
	printf("%d\n",cnt);

	return 0;
}