#include <iostream>
#include <cstdio>

using namespace std;

int main(void)
{
	pair<int,int> arr[3];

	for (int i=0;i<3;i++)
		scanf("%d%d",&(arr[i].first),&(arr[i].second));

	pair<int,int> vec[2];

	vec[0].first=arr[1].first-arr[0].first;
	vec[0].second=arr[1].second-arr[0].second;
	vec[1].first=arr[2].first-arr[1].first;
	vec[1].second=arr[2].second-arr[1].second;

	int value = - vec[1].first*vec[0].second + vec[0].first*vec[1].second;
	if (value == 0)
		printf("0\n");
	else if (value >0)
		printf("1\n");
	else
		printf("-1\n");

	return 0;
}