#include <stdio.h>

char line[10];

int main(void)
{
	int test;
	freopen("test.txt","r",stdin);
	scanf("%d",&test);

	for (int i=0;i<test;i++)
	{
		int rc;
		bool res = true;
		scanf("%d",&rc);
		for (int j=0;j<rc;j++)
		{
			scanf("%s",line);
			if ( (line[0]==line[1] && line[0]=='0')
				|| (line[1]==line[2] && line[2]=='0') )
				res = false;
		}
		printf("Case %d: ",i+1);
		if (!res)
			printf("Fallen\n");
		else
			printf("Standing\n");
	}

	return 0;
}