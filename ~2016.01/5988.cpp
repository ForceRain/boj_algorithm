#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

char input_buffer[100];

int main(void)
{
	int t,len;
	scanf("%d",&t);
	for (int i=0;i<t;i++)
	{
		scanf("%s",input_buffer);
		len = strlen(input_buffer);
		if ((input_buffer[len-1]-'0')%2)
			printf("odd\n");
		else
			printf("even\n");
	}
	return 0;
}