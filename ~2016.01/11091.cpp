#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>

using namespace std;

char input_buffer[1000];

int main(void)
{
	int test;
	scanf("%d",&test);
	getchar();
	for (int i=0;i<test;i++)
	{
		int check[256]={0};
		vector<char> container;
		gets(input_buffer);
		
		int len = strlen(input_buffer);
		for (int j=0;j<len;j++)	
			check[ input_buffer[j] ]++;
		
		for (int j=0;j<256;j++)	
			if ((check[j]==0) && (check[j-('a'-'A')]==0) && ('a'<=j && j<='z'))
				container.push_back(j);

		if (container.empty())
			printf("pangram\n");
		else
		{
			printf("missing ");
			int sz=container.size();
			for (int j=0;j<sz;j++)	printf("%c",container[j]);
			printf("\n");
		}
	}

	return 0;
}