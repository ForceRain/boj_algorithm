#include <iostream>
#include <cstdio>

using namespace std;

int main(void)
{
	int test;
	scanf("%d",&test);
	long long K,N;

	for (int i=0;i<test;i++)
	{
		scanf("%lld%lld",&K,&N);
		printf("%lld %lld %lld %lld\n",K,(N*(N+1))/2,N*N,N*N+N);
	}

	return 0;
}