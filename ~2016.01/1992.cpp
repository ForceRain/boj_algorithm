#include <iostream>
#include <cstdio>
#include <stack>

using namespace std;

char q_space[100][100];
stack <char> container;

int divide_and_conquer(int st_x,int st_y,int lev)
{
	int len = lev/2;
	cout<<st_x<<","<<st_y<<" lev :"<<lev;
	if ( lev == 1 )
	{ 
		cout<<", ret : "<<q_space[st_x][st_y]<<endl;
		return (q_space[st_x][st_y]=='1')?1:0;
	}
	else
	{
		cout<<endl;
		container.push(')');
		int r4 = divide_and_conquer(st_x+len,st_y+len,len);
		if (r4>=0)	container.push(r4+'0');
		int r3 = divide_and_conquer(st_x+len,st_y,len);
		if (r3>=0)	container.push(r3+'0');
		int r2 = divide_and_conquer(st_x,st_y+len,len);
		if (r2>=0)	container.push(r2+'0');
		int r1 = divide_and_conquer(st_x,st_y,len);
		if (r1>=0)	container.push(r1+'0');
		container.push('(');
				
		if ( ( r1==1 && r2==1 && r3==1 && r4==1 ) || ( r1==0 && r2==0 && r3==0 && r4==0 ) )
		{
		//	printf("%d",r1);
			return r1;
		}
		else
		{
		/*	printf("(");
			if (r1>=0)	printf("%d",r1);
			if (r2>=0)	printf("%d",r2);
			if (r3>=0)	printf("%d",r3);
			if (r4>=0)	printf("%d",r4);
			printf(")");*/
			return -100;
		}
	}
}

int main(void)
{
	int N;
	freopen("test.txt","r",stdin);
	scanf("%d",&N);
	for (int i=0;i<N;i++)
		scanf("%s",q_space[i]);
	
	bool all_check = true;
	int val = q_space[0][0];
	for (int i=0;i<N;i++)	for (int j=0;j<N;j++)	if ( val != q_space[i][j] )	all_check=false;
	
	if (!all_check){
		divide_and_conquer(0,0,N);
		int sz = container.size();
		for (int i=0;i<sz;i++){
			printf("%c",container.top());
			container.pop();
		}
	}
	else
		printf("%d",val-'0');

	return 0;
}