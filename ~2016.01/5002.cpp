#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>

using namespace std;

char input_buffer[101];

int main(void)
{
	int N,len;
	scanf("%d",&N);
	scanf("%s",input_buffer);
	len = strlen(input_buffer);
	int Wc=0,Mc=0;
	bool cut = false;
	int res = 0;

	for (int i=0;i<len;i++)
	{
		if (input_buffer[i]=='W')	Wc++;
		else		Mc++;
		if ( abs(Wc-Mc)>N )	
		{
			res = Wc+Mc;
			cut = true;
			break;
		}
	}
	if (!cut)
		printf("%d\n",len);
	else
		printf("%d\n",res);
	
	return 0;
}