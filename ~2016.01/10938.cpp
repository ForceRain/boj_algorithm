#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

int intermid[1001];
char input_stream[101];
char mapping[32];

int main(void)
{
	for (int i=0;i<1001;i++)	intermid[i]=-1;
	for ( int j = 0; j < 6; j++ )	mapping[j+26] = j+'2';
	for ( int k = 0; k < 26; k++ )	mapping[k] = k+'A';
	freopen("test.txt","r",stdin);

	scanf("%s",input_stream);
	int len = strlen(input_stream);
	int transPtr = 0 ;

	for ( int i = 0 ; i < len ; i++)
	{
		int ruler = 128;
		for ( int j = 0; j < 8; j++ )
		{
			intermid[transPtr++] = input_stream[i]/ruler;
			input_stream[i] = input_stream[i]%ruler;
			ruler = ruler/2;
		}
	}

	int sz = ((len*8)%40==0)?len*8:( (((len*8)/40)+1)*40 );
	for ( int k = 0; k < sz ; k += 5 )
	{
		bool min_find = false;
		bool ex_val = false;
		for ( int j = k; j < k+5 ; j++ )
		{
			if ( intermid[j] == -1 )
				min_find = true;
			if ( intermid[j] != -1 )
				ex_val = true;
		}
		if ( !ex_val && min_find )
		{
			printf("=");
			continue;
		}
		else if ( ex_val && min_find )
		{
			for ( int j = k; j < k+5 ; j++ )
			{
				if ( intermid[j] == -1)
					intermid[j] = 0;
			}
		}
		int val = 16 * intermid[k] + 8 * intermid[k+1] + 4 * intermid[k+2] + 2 * intermid[k+3] + intermid[k+4];
		printf("%c",mapping[val]);
	}
	printf("\n");

	return 0;
}