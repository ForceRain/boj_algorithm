#include <iostream>
#include <cstdio>

using namespace std;

int main(void)
{
	int N;
	scanf("%d",&N);
	int val,bef=-1;
	int max=-1,len=0;
	int lb=0;
	for (int i=0;i<N;i++)
	{
		scanf("%d",&val);
		if (i == 0){
			bef = val;
			lb = val;
			continue;
		}

		if (bef<val)
			len=val-lb;
		else
		{
			if (max<len)
				max=len;
			lb=val;
		}
		bef=val;
	}
	if (N==1)
		max = 0;
	if (max<len)
		max=len;

	printf("%d\n",max);

	return 0;
}