#include <iostream>
#include <cstdio>

using namespace std;

int values[1001];
int dp[1001];

int main(void)
{
	int N;
//	freopen("test.txt","r",stdin);
	for (int i=1;i<1001;i++)	dp[i]=12345678;
	scanf("%d",&N);
	for (int i=0;i<N;i++)
		scanf("%d",&values[i]);

	for (int j=0;j<N;j++)
	{
		int val = values[j];
		for (int k=1;k<=val;k++)
		{
			if ( (j+k)<N && dp[j+k]>(dp[j]+1))
				dp[j+k]=dp[j]+1;
		}
	}
	if (dp[N-1]==12345678)
		printf("-1\n");
	else
		printf("%d\n",dp[N-1]);

	return 0;
}