#include <iostream>
#include <cstdio>
#include <cstring>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

char input_stream[500001];
vector < pair< string,int> > container; 
int LCP[500001];

bool my_compare( const pair<string,int> &a, const pair<string,int> &b  )
{
	if ( a.first < b.first)
		return true;
	else return false;
}

int main(void)
{
	int N;
	freopen("test.txt","r",stdin);

	scanf("%s",input_stream);
	int len = strlen(input_stream);
	
	for (int i=0;i<len;i++)
		container.push_back( pair<string,int> ( input_stream+i,i+1 ) );

	sort(container.begin(),container.end(),my_compare);

	LCP[1] = -1;
	printf("%d",container[0].second);

	for ( int i=1;i<len;i++ )
	{
		printf(" %d",container[i].second);
		pair<string,int> bef = container[i-1];
		pair<string,int> tmp = container[i];
		string t1 = bef.first; int l1 = t1.length();
		string t2 = tmp.first; int l2 = t2.length();
		int cnt = 0;
		while ( cnt < l1 && cnt < l2 && t1[cnt] == t2[cnt])	cnt++;
		LCP[i] = cnt;
	}
	printf("\nx");
	for ( int i=1; i<len;i++)
		printf(" %d",LCP[i]);
	printf("\n");
	
	return 0;
}