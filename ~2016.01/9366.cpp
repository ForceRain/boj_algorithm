#include <iostream>
#include <cstdio>
#include <algorithm>

using namespace std;

int main(void)
{
	int N;
	scanf("%d",&N);

	for (int i=0;i<N;i++)
	{
		int arr[3];
		scanf("%d%d%d",&arr[0],&arr[1],&arr[2]);
		sort(arr,arr+3);

		if ( arr[0]+arr[1] <= arr[2] )
			printf("Case #%d: invalid!\n",i+1);
		else
		{
			if ( arr[0]==arr[1] && arr[1]==arr[2] )
				printf("Case #%d: equilateral\n",i+1);
			else if ( (arr[0]==arr[1] && arr[1]!=arr[2]) || 
				( arr[0]!=arr[1] && arr[1]==arr[2] ) )
				printf("Case #%d: isosceles\n",i+1);
			else
				printf("Case #%d: scalene\n",i+1);
		}
	}

	return 0;
}