#include <iostream>
#include <cstdio>
#include <algorithm>
#include <vector>

using namespace std;

bool visited[102][102];
vector <int> container;
int N;
int M;
int cnt = 0 ;
int position[4][2] = { { 0, 1 },{ 0 , -1 },{ 1 , 0 },{ -1 , 0 } };

int DFS_Visit(int x, int y)
{
	int value = 1;
	for ( int i = 0; i < 4 ;i++ )
	{
		int nx = position[i][0]+x;
		int ny = position[i][1]+y;
		if ( ( 0 <= nx ) && ( nx < M ) && ( 0 <= ny ) && ( ny < N ) )
		{
			if ( !visited[nx][ny] )
			{
				visited[nx][ny] = true;
				value += DFS_Visit(nx,ny);
			}
		}
	}
	return value;
}

void DFS()
{
	for ( int i = 0 ; i < M ; i++ )
	{
		for ( int j = 0 ; j < N ; j++ )
		{
			if ( !visited[i][j] )
			{
				visited[i][j] = true;
				int sz = DFS_Visit(i,j);
				cnt++;
				container.push_back(sz);
			}
		}
	}
}

int main(void)
{
	int K;
//	freopen("test.txt","r",stdin);
	scanf("%d%d%d",&M,&N,&K);				// left,up pos = 1,1

	for (int l=0;l<K;l++)
	{
		int sx,sy,ex,ey;
		scanf("%d%d%d%d",&sx,&sy,&ex,&ey);
		for (int i=sy ; i<ey ; i++)
			for ( int j=sx ; j<ex ; j++ )
				visited[i][j]=true;
	}

	DFS();
	sort(container.begin(),container.end());

	int sz = container.size();
	printf("%d\n",sz);
	for ( int i=0;i<sz;i++ )
		printf("%d ",container[i]);
	printf("\n");

	return 0;
}