#include <iostream>
#include <cstdio>
#include <vector>

using namespace std;

char mapping[1001][1001];
char input_buffer[1001];

int main(void)
{
	int N;
	freopen("test.txt","r",stdin);
	while (true)
	{
		vector <pair<int,int> > hashMapContainer;
		scanf("%d",&N);
		if (N==0)
			break;
		for (int i=0;i<N;i++)
		{
			scanf("%s",input_buffer);
			for (int j=0;j<N;j++)
			{
				if (input_buffer[j]=='O')
					hashMapContainer.push_back(pair<int,int>(i,j));
			}
		}
		for (int i=0;i<N;i++)
			scanf("%s",mapping[i]);

		int sz = hashMapContainer.size();
		for (int j=0;j<sz;j++)
			printf("%c",mapping[hashMapContainer[j].first][hashMapContainer[j].second]);

		for (int i=0;i<3;i++){
			for (int j=0;j<sz;j++)
			{
				if (i==0)
					printf("%c",mapping[hashMapContainer[j].second][(N-1)-hashMapContainer[j].first]);
				else if (i==1)
					printf("%c",mapping[(N-1)-hashMapContainer[j].first][(N-1)-hashMapContainer[j].second]);
				else
					printf("%c",mapping[(N-1)-hashMapContainer[j].second][hashMapContainer[j].first]);
			}
		}
		printf("\n");
	}
	return 0;
}