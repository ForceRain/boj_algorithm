#include <iostream>
#include <vector>
#include <cstdio>
#include <string>
#include <cstring>
#include <algorithm>

using namespace std;

char input_buffer[1001];
vector <string> container;

int main(void)
{
	scanf("%s",input_buffer);
	int len = strlen(input_buffer);

	for (int i=0;i<len;i++)
		container.push_back(string(input_buffer+i));
	sort(container.begin(),container.end());

	int sz = container.size();
	for (int i=0;i<sz;i++)
		printf("%s\n",container[i].c_str());

	return 0;
}