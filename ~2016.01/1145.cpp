#include <iostream>
#include <cstdio>

using namespace std;

int main(void)
{
	int arr[5];
	for (int i=0;i<5;i++)
		scanf("%d",&arr[i]);

	int runner = 0;
	int cnt = 0;
	do
	{
		runner++;
		cnt = 0;
		for (int i=0;i<5;i++)
			if ( (runner%arr[i])==0 )	cnt++;		
	}
	while ( cnt<3 );
	printf("%d\n",runner);

	return 0;
}