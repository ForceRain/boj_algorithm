#include <iostream>
#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>

using namespace std;

int smap[1001][1001];
int visited[1001];
int f_time[1001];
int N;
int cnt,f_t;
enum {W,B,G};
vector < pair<int,int> > container;

void DFS_Visit(int nd_num)
{
	for (int i=0;i<=N;i++)
	{
		if ( smap[nd_num][i]!=0 && visited[i]==W )
		{
			visited[i]=G;
			DFS_Visit(i);
			visited[i]=B;
		}
	}
	f_time[nd_num]=f_t++;
}

void DFS(int ver)
{
	if ( ver == 1 )
	{
		int sz = container.size();
		for (int i=0;i<sz;i++)
		{
			int pos = container[i].second;
			if ( visited[pos]==W )
			{
				visited[pos]=G;
				DFS_Visit(pos);
				visited[pos]=B;
				cnt++;
			}
		}
	}
	else
	{
		for (int i=0;i<=N;i++)
		{
			if ( !visited[i] )
			{
				visited[i]=G;
				DFS_Visit(i);
				visited[i]=B;
			}
		}
	}
}

int main(void){
	int M;
//	freopen("test.txt","r",stdin);
	scanf("%d%d",&N,&M);
	cnt=0;

	for (int i=0;i<M;i++){
		int a,b;
		scanf("%d%d",&a,&b);
		smap[a][b]=1;
		smap[b][a]=1;
	}
	//DFS
	DFS(0);
	for (int i=1;i<=N;i++)
		container.push_back(pair<int,int>(f_time[i],i));
	sort(container.begin(),container.end());
	
	//transpose
	
	//DFS -> get number here;
	memset(visited,0,sizeof(visited));
	DFS(1);
	printf("%d\n",cnt);

	return 0;
}