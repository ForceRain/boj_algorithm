#include <iostream>
#include <cstdio>

using namespace std;

int dp[10000001];
int values[10001];

int main(void)
{
	int N,M;
	scanf("%d",&N);
	scanf("%d",&M);

	for (int i=1;i<=N;i++){
		scanf("%d",&values[i]);
		dp[values[i]]=1;
	}
	dp[0]=1;
	int cnt=0;
	for (int i=0;i<=N;i++)
	{
		if ( (M-values[i])!=values[i] && (dp[M-values[i]]==1) )
			cnt++;
	}

	printf("%d\n",cnt/2);

	return 0;
}