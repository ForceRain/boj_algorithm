#include <iostream>
#include <cstdio>

using namespace std;

int main(void)
{
	int N;
	freopen("test.txt","r",stdin);
	scanf("%d",&N);

	for (int i=0;i<N;i++)
	{
		int num;
		scanf("%d",&num);
		int t=0;
		while ( t*t+t <= num )
			t++;
		printf("%d\n",t-1);
	}

	return 0;
}