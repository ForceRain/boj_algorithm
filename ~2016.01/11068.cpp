#include <iostream>
#include <cstdio>

using namespace std;

int arr[1001];

int main(void)
{
	int test;
	freopen("test.txt","r",stdin);
	scanf("%d",&test);

	for (int i=0;i<test;i++)
	{
		int val;
		scanf("%d",&val);
		bool check = false;

		for (int j=2;j<=64;j++)
		{
			int moving = val;
			int checkPtr = 0;
			bool palCheck = true;
			while ( moving >= j )
			{
				arr[checkPtr++]=moving%j;
				moving = moving / j;
			}
			arr[checkPtr]=moving;
			for ( int k=0; k<=checkPtr;k++ )
			{
				if ( arr[k] != arr[checkPtr-k] ){
					palCheck = false;
					break;
				}
			}
			if (palCheck){
				check = true;
				break;
			}
		}
		if ( check )
			printf("1\n");
		else
			printf("0\n");
	}

	return 0;
}