#include <iostream>
#include <cstdio>
#include <vector>

using namespace std;

vector<int> p_container;
int primes[1001];
int space[10001];

int main(void)
{
	int T,N;
	scanf("%d",&T);

	for (int i=2;i<1000;i++)
	{
		for (int j=2*i;j<1000;j+=i)
			primes[j]=1;
	}
	for (int i=2;i<1000;i++)
	{
		if (primes[i]==0)
		{
			p_container.push_back(i);
			space[i]=i;
		}
	}
	int sz=p_container.size();
	for (int i=2;i<10000;i++)
	{
		for (int j=0;j<sz;j++)
		{
			if (i-p_container[j]>=0)
			{
				if ( (space[i-p_container[j]]!=0) && (space[i] == 0) )
					space[i]=p_container[j];
			}
		}
	}/*
	for (int i=2;i<10000;i++)
	{
		if (space[i]==i)
			space[i]=0;
	}*/
	for (int i=0;i<T;i++)
	{
		bool out=false;
		scanf("%d",&N);
		for (int j=0; (j<sz) && (!out) ;j++)
		{
			if ( N-p_container[j]>=0 )
			{
				if ( space[N-p_container[j]]!=0 && space[N-p_container[j]-space[N-p_container[j]]]!=0 )
				{
					if ( (p_container[j]+space[N-p_container[j]]+space[N-p_container[j]-space[N-p_container[j]]]) == N)
					{
						printf("%d %d %d\n",p_container[j],
							space[N-p_container[j]],
							space[N-p_container[j]-space[N-p_container[j]]]);
						out=true;
						break;
					}
				}
			}
		}
		if (!out)
			printf("0\n");
	}

	return 0;
}