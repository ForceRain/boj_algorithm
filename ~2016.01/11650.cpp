#include <iostream>
#include <cstdio>
#include <algorithm>
#include <vector>

using namespace std;

vector < pair<int,int> > container;

bool my_compare(pair<int,int> a, pair<int,int> b)
{
	if (a.first<b.first)
		return true;
	else if (a.first>b.first)
		return false;
	else
		return a.second<b.second;
}

int main(void)
{
	int N;
	scanf("%d",&N);

	for (int i=0;i<N;i++)
	{
		int a,b;
		scanf("%d%d",&a,&b);
		container.push_back(pair<int,int>(a,b));
	}
	sort(container.begin(),container.end(),my_compare);

	for (int i=0;i<N;i++)
		printf("%d %d\n",container[i].first,container[i].second);

	return 0;
}