#include <iostream>
#include <cstdio>

using namespace std;

int space[101][101];

int main(void)
{
	int N,M;
	freopen("test.txt","r",stdin);
	scanf("%d%d",&N,&M);

	for ( int i=0;i<M;i++ )
	{
		int a,b;
		scanf("%d%d",&a,&b);
		space[a][b]=1;
		space[b][a]=1;
	}

	for ( int k=1;k<=N;k++ )
	{
		for ( int i=1;i<=N;i++ )
		{
			for ( int j=1;j<=N;j++ )
			{
				if ( (space[i][k]!=0) && (space[k][j]!=0) && (i!=j))
				{
					if ( space[i][j]==0 )
						space[i][j] = space[i][k] + space[k][j];
					else if ( space[i][j] > space[i][k] + space[k][j] )
						space[i][j] = space[i][k] + space[k][j];
				}
			}
		}
	}

	int rowNum = -1;
	int min = 0x7FFFFFFF;
	for ( int i=1;i<=N;i++ )
	{
		int summation = 0;
		for ( int j=1;j<=N;j++ )
		{
			if ( i!=j )
				summation += space[i][j];
		}
		
		if ( summation < min )
		{
			rowNum = i;
			min = summation;
		}
	}
	printf("%d\n",rowNum);

	return 0;
}