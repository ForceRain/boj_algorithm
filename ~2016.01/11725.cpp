#include <iostream>
#include <cstdio>
#include <vector>
#include <queue>

using namespace std;

vector<int> space[100001];
queue<int> container;
int parent[100001];
bool visited[100001];

int main(void)
{
	int N;
	freopen("test.txt","r",stdin);
	scanf("%d",&N);
	int tot_size = N-1;
	for (int i=0;i<tot_size;i++)
	{
		int a,b;
		scanf("%d%d",&a,&b);
		space[a].push_back(b);
		space[b].push_back(a);
	}
	visited[1]=true;
	container.push(1);
	while (!container.empty())
	{
		int src = container.front();
		container.pop();
		int sz = space[src].size();
		for (int i=0;i<sz;i++)
		{
			if (!visited[space[src][i]])
			{
				visited[space[src][i]]=true;
				parent[space[src][i]]=src;
				container.push(space[src][i]);
			}
		}
	}
	for (int i=2;i<=N;i++)
		printf("%d\n",parent[i]);

	return 0;
}