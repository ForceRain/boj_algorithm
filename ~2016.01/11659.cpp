#include <iostream>
#include <cstdio>

using namespace std;

int ac_sum[100003];

int main(void)
{
	int N,M;
	freopen("test.txt","r",stdin);
	scanf("%d%d",&N,&M);
	int ac=0;
	for (int i=1;i<=N;i++)
	{
		int val;
		scanf("%d",&val);
		ac+=val;
		ac_sum[i]=ac;
	}
	
	for (int i=0;i<M;i++)
	{
		int f,t;
		scanf("%d%d",&f,&t);
		if (f>t)
		{
			int tmp = f;
			f = t;
			t = tmp;
		}
		printf("%d\n",ac_sum[t]-ac_sum[f-1]);
	}

	return 0;
}