#include <iostream>
#include <cstdio>

using namespace std;

char input_stream[1010];
char mapping[256];

int main(void)
{
	int start='D';
	for (int i='A';i<='Z';i++)
	{
		mapping[start++]=i;
		if (!(('A'<= start) && (start<='Z')))
			start='A';
	}
	scanf("%s",input_stream);
	int len = strlen(input_stream);
	for (int i=0;i<len;i++)
		printf("%c",mapping[input_stream[i]]);
	printf("\n");

	return 0;
}